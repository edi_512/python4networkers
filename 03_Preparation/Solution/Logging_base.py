# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

import logging


def logger_config():
    # Create a logger Beispiel_Log
    new_logger = logging.getLogger('Beispiel_Log')

    # Set log level
    new_logger.setLevel(logging.DEBUG)

    # filehandle.setLevel(logging.INFO)
    # Format the log entries
    log_format = logging.Formatter(('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    # Create a log file for the logger
    filehandle = logging.FileHandler('outfiles/debug.log')
    # Assign log_format to the log file
    filehandle.setFormatter(log_format)
    # Assign the filehandler to the logger
    new_logger.addHandler(filehandle)
    return new_logger


def test_the_logger(logger):
    # create test log messages
    # > depending on the log level set, you'll see all or a subset in the log file
    logger.debug('Test Debug-Level')
    logger.info('Test Info-Level')
    logger.warning('Test Warning-Level')
    logger.error('Test Error-Level')
    logger.critical('Test Critical-Level')
    a = 0
    while a < 5:
        a += 1
        logger.debug(f'looped for {a} times')
        if a >= 3:
            # crash the script to verify no log message is lost
            c = a / 0


if __name__ == "__main__":
    logger = logger_config()
    test_the_logger(logger)

