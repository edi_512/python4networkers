# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

from Credentials import switch_credentials


def get_device():
    device_list = switch_credentials.dev_list['devices']

    for device in device_list:
        device_type = device['device_type']
        host = device['host']
        username = device['username']
        password = device['password']
        print ('Connecting to', host, 'as', device_type, 'with:'
               '\n - user:', username,
               '\n - pass:', password)
    print ("all devices listed")


if __name__ == '__main__':
    get_device()

