#!/usr/bin/env python
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

import json


def print_title(title):
    print('\n')
    print(title)
    print('-' * len(title))


def main():
    # open file and read content
    int_list_file = open('infiles/Interface_List.json','r')
    int_list_json = int_list_file.read()
    type_int_list_json = type(int_list_json)

    print_title('type of var int_list_json')
    print(type_int_list_json)
    print_title('content of int_list_json')
    print(int_list_json)

    # convert int_list_json to a dictionary
    print_title('converting int_list to Python objects')
    int_list_dict = json.loads(int_list_json)
    print_title('type of int_list_dict')
    print(type(int_list_dict))

    # print the entire content of the Python objet int_list_dict
    print_title('conntent of int_list_dict')
    print(int_list_dict)

    # print the response part only in a nice pretty fashion
    print_title('Display the response part only')
    int_list_dict_snippet = int_list_dict['response']
    print(json.dumps(int_list_dict_snippet,indent=2))
    print('')

    # loop through the dictionary and print Interfacename, Description and Status
    print_title('loop trough the dictionary')
    for interface in int_list_dict_snippet:
        print(' Interface: ' + interface['portName'] + ' Description: ' + interface['description'] + ' Status: '+ interface['status'])

    # direct access of the portName of the first device
    print_title('direct access of the interface name of the first interface')
    print(int_list_dict['response'][0]['portName'])


if __name__ == "__main__":
    main()



