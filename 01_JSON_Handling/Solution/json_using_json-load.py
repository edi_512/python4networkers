import json
# open json file
int_list_file = open('infiles/Interface_List.json','r')

# load the content in one step into python data structures
interface_list = json.load(int_list_file)

print('The file content:')
print(json.dumps(interface_list, indent=2))
response_only = interface_list['response']

# print a list of Interface Names (PortName)
print('\nList of interface names:')
for interface in response_only:
    print(interface['portName'])

# write the list of interface - without response and version - nicely formatted into a json formatted file
out_file = open('outfiles/Interface_List_out.json','w')
out_file.write(json.dumps(response_only, indent=4))
