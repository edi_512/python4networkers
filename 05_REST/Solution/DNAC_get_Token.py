#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

import requests
from requests.auth import HTTPBasicAuth
from Credentials import catc_credentials

# Disable invalid certificate warnings.
requests.packages.urllib3.disable_warnings()


def get_token():
    # prepare credentials
    hostname = catc_credentials.device['hostname']
    device_ip = catc_credentials.device['ip']
    user = catc_credentials.device['user']
    passw = catc_credentials.device['pass']

    # get token
    url = "https://" + device_ip + "/dna/system/api/v1/auth/token"
    token_header = {'content-type': 'application/json'}
    response = requests.post(url, auth=HTTPBasicAuth(username=user, password=passw), headers=token_header, verify=False)
    catc_token = response.json()['Token']
    print('Working on:', hostname)
    return catc_token


if __name__ == '__main__':
    token = get_token()
    print(token)

