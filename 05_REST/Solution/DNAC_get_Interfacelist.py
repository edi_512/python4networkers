#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

import requests
from requests.auth import HTTPBasicAuth
from Credentials import catc_credentials

# Disable invalid certificate warnings.
requests.packages.urllib3.disable_warnings()


def get_token():
    # prepare credentials
    hostname = catc_credentials.device['hostname']
    device_ip = catc_credentials.device['ip']
    user = catc_credentials.device['user']
    passw = catc_credentials.device['pass']

    # get token
    url = "https://" + device_ip + "/dna/system/api/v1/auth/token"
    token_header = {'content-type': 'application/json'}
    response = requests.post(url, auth=HTTPBasicAuth(username=user, password=passw), headers=token_header, verify=False)
    catc_token = response.json()['Token']
    print('Working on:', hostname)
    return catc_token


def get_interfacelist(base_url):
    token = get_token()
    url_addon = 'interface'
    url = base_url + url_addon
    my_headers = {'x-auth-token': token}
    response = requests.get(url, headers = my_headers, verify = False)
    data = response.json()
    print(data)
    # filter the response part
    interface_list = data['response']
    # print out some parameters for each interface
    #
    for interface in interface_list:
        print(f"Host-ID: {interface['deviceId']}")
        print(f"Interface: {interface['portName']}")
        print(f"Status: {interface['status']}")
    return interface_list


def get_devicelist(base_url):
    token = get_token()
    url_addon = 'network-device'
    url = base_url + url_addon
    my_headers = {'x-auth-token': token}
    response = requests.get(url, headers=my_headers, verify=False)
    data = response.json()
    # filter the response part
    device_list = data['response']
    # print out some parameters for each device
    for device in device_list:
        print('Hostname: %s' % device['hostname'])
        print('ID: %s' % device['id'])
    return device_list

    
def prepare_outfile():
    fobj_out = open("./outfiles/InterfaceList.csv","w")
    fobj_out.write('Host_Name;Interface_Name;Status;PortMode\n')
    return fobj_out
    

def create_full_list_nested(device_list, interface_list):
    # prepare the out file using the function prepare_outfile
    fobj_out = prepare_outfile()
    print('\nAlle Angaben\n')
    # iterate through both lists to find the device name for each interface
    for interface in interface_list:
        Dev_ID_Int = interface['deviceId']
        Host_Name = ""
        for device in device_list:
            Dev_ID_Dev = device['id']
            if Dev_ID_Int == Dev_ID_Dev:
                Host_Name = device['hostname']
        line = Host_Name+';'+interface['portName']+';'+interface['status']+';'+ str(interface['portMode'])
        print(line)
        fobj_out.write(line + '\n')
    fobj_out.close()


def create_full_list_dictionary(device_list, interface_list):
    # prepare the out file using the function prepare_outfile
    fobj_out = prepare_outfile()
    print('\nAlle Angaben\n')
    # create the dictionary containing device-id as key and hostname as value
    device_dict = {}
    for device in device_list:
        device_dict[device['id']] = device['hostname']
    #
    for interface in interface_list:
        Dev_ID_Int = interface['deviceId']
        Host_Name = device_dict[Dev_ID_Int]
        line = (f"{Host_Name};{interface['portName']};{interface['status']};{str(interface['speed'])}")
        print (line)
        fobj_out.write(line + '\n')
    fobj_out.close()


def main():
    base_url = 'https://172.22.124.253/dna/intent/api/v1/'
    int_list = get_interfacelist(base_url)
    dev_list = get_devicelist(base_url)
    # create_full_list_nested(dev_list, int_list)
    create_full_list_dictionary(dev_list, int_list)


if __name__ == '__main__':
    main()
