#! /usr/bin/env python
import traceback
import lxml.etree as etree
from ncclient import manager
from ncclient.operations import RPCError
from Credentials import netconf_credentials

# "Command"-Template
payload = [
'''
<edit-config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <target>
    <running/>
  </target>
  <config>
    <interfaces xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
      <interface>
        <name>Loopback0</name>
        <description>Hello from NETCONF</description>
      </interface>
    </interfaces>
  </config>
</edit-config>
'''
]


def load_credentials():
    # prepare credentials
    hostname = netconf_credentials.device['hostname']
    device_ip = netconf_credentials.device['ip']
    port_nr = netconf_credentials.device['portnr']
    user = netconf_credentials.device['user']
    passw = netconf_credentials.device['pass']

    return hostname, device_ip, port_nr, user, passw


def connection_setup():
    hostname, device_ip, port_nr, user, passw = load_credentials()
    # connect to netconf agent
    print('connecting to ', hostname)
    my_session = manager.connect(host=device_ip,
                         port=port_nr,
                         username=user,
                         password=passw,
                         timeout=90,
                         hostkey_verify=False,
                         device_params={'name': 'csr'})
    return my_session


def send_rpc(my_session):
    # execute netconf operation
    for rpc in payload:
        try:
            response = my_session.dispatch(etree.fromstring(rpc))
            data = response.xml
        except RPCError as e:
            data = e.xml
            pass
        except Exception:
            traceback.print_exc()
            exit(1)

        # beautify output
        if etree.iselement(data):
            data = etree.tostring(data, pretty_print=True).decode()

        try:
            nice_output = etree.tostring(
                etree.fromstring(data.encode('utf-8')),
                pretty_print=True
            ).decode()
        except Exception:
            traceback.print_exc()
            exit(1)

        print(nice_output)


if __name__ == '__main__':
    session = connection_setup()
    send_rpc(session)
