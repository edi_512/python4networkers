#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

# This Script connects to a Cisco IOS-XE

from ncclient import manager
from lxml import etree as et
import xmltodict
import sys
import traceback

from Credentials import netconf_credentials

# prepare the filter
filter_hostname = '''
<get xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <filter>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <hostname></hostname>
    </native>
  </filter>
</get> 
'''

def main():
    # prepare credentials
    hostname = netconf_credentials.device['hostname']
    device_ip = netconf_credentials.device['ip']
    port_nr = netconf_credentials.device['portnr']
    user = netconf_credentials.device['user']
    passw = netconf_credentials.device['pass']

    # connect to device
    try:
        session = manager.connect(host=device_ip,
                                     port=port_nr,
                                     username=user,
                                     password=passw,
                                     hostkey_verify=False,
                                     device_params={'name': 'csr'},
                                     allow_agent=False,
                                     look_for_keys=False
                                     )
    except Exception as cause:
        print('Failed to connect - cause:\n', cause)
        sys.exit()
    get_hostname(session, hostname)
    session.close_session()


def get_hostname(my_session, hostname):
    print('Connecting to: ', hostname)

    # send the RPC request
    response = my_session.dispatch(et.fromstring(filter_hostname))
    print('Type of response: ', type(response))

    # print the entire response nicely
    beautify_xml_output(response)

    # Filter the hostname: convert xml to dictionary and filter for hostname
    hostname_dict = xmltodict.parse(str(response))
    hostname_only = hostname_dict["rpc-reply"]["data"]["native"]["hostname"]
    print('Hostname: ', hostname_only)


def beautify_xml_output(response):
    # convert the response to a string
    data_str = response.xml
    print('Type of data: ', type(data_str), '\n')
    # note: the output is one line --> not pretty
    print('response convert to text looks like this:\n', data_str, '\n')

    # beautify output
    # two steps:
    # - convert from string to xml object taking care of the encoding
    # - convert xml back to string and turn on pretty_print
    try:
        data_xml = et.fromstring(data_str.encode('utf-8'))
        print('Type of data_xml: ', type(data_xml))
        data_nice_xml = et.tostring(data_xml, pretty_print=True).decode()
        print('Type of data_nice_xml: ', type(data_nice_xml))

    except Exception as e:
        traceback.print_exc()
        exit(1)

    print(data_nice_xml)

if __name__ == '__main__':
    main()
