#! /usr/bin/env python
import traceback
import lxml.etree as et
from ncclient import manager
from ncclient.operations import RPCError
from Credentials import netconf_credentials

# prepare credentials
hostname = netconf_credentials.device['hostname']
device_ip = netconf_credentials.device['ip']
port_nr = netconf_credentials.device['portnr']
user = netconf_credentials.device['user']
passw = netconf_credentials.device['pass']

payload = [
'''
<get xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <filter>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
      <interface>
        <Loopback>
          <name>0</name>
        </Loopback>
      </interface>
    </native>
  </filter>
</get> 
''',

'''
<get xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <filter>
    <interfaces xmlns="http://openconfig.net/yang/interfaces">
      <interface>
        <config>
          <name/>
        </config>
      </interface>
    </interfaces>
  </filter>
</get>
'''
]


if __name__ == '__main__':
    # connect to netconf agent
    with manager.connect(host=device_ip,
                         port=port_nr,
                         username=user,
                         password=passw,
                         timeout=90,
                         hostkey_verify=False,
                         device_params={'name': 'csr'}) as m:

        # execute netconf operation
        for rpc in payload:
            try:
                response = m.dispatch(et.fromstring(rpc))
                data = response.xml
            except RPCError as e:
                data = e.xml
                pass
            except Exception as e:
                traceback.print_exc()
                exit(1)

            # beautify output
            if et.iselement(data):
                data = et.tostring(data, pretty_print=True).decode()

            try:
                out = et.tostring(
                    et.fromstring(data.encode('utf-8')),
                    pretty_print=True
                ).decode()
            except Exception as e:
                traceback.print_exc()
                exit(1)

            print(out)
