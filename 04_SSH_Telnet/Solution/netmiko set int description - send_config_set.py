#!/usr/bin/env python

'''
Goal:
This script demonstrates how netmiko send_config_set behaves if a command from the command list fails.
Preparation:
On the switch under test make sure on int g1/0/22 no description is present
Workflow:
On the switch perform 'sh int descr'
run the script
    the int g1/0/63 will fail: % Invalid input detected at '^' marker.
    notice: the description for int 63 is sent ('descr netmiko') even though int 63 doesn't exist
On the switch perform 'sh int descr'
    - notice: description on int g1/0/22 is configured - order was int 63 then int 22
'''

import sys
import netmiko
from Credentials import switch_credentials

commands = ["int g1/0/63", "descr netmiko", "int g1/0/22", "descr netmiko ignored errors"]


def conf_device(device):
    # setup connection
    with netmiko.ConnectHandler(**device) as net_connect:
        print('connected to:', device['host'])
        output = net_connect.send_config_set(commands)
        for line in output.splitlines():
            print(line)
            if '%' in output:
                print('Somthing went wrong - script aborted\n', output)
                sys.exit()

    print('\n', output)


if __name__ == "__main__":
    # load the dictionary with credentials from file 'Credentials/switch_credentials'
    device_list = switch_credentials.dev_list['devices']
    # loop through the list of devices
    for device in device_list:
        # actions to perform on the device
        print('Working on', device['host'])
        conf_device(device)
    print('all done')
