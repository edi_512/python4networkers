# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

'''
Goal:
This script demonstrates the use of netmiko to configure Cisco switches using ssh
Details:
The list of devices is stored in a file outside the script and imported.
The script loops over all device listed in the mentioned file
On each device the interface description for two interfaces is set. In order to have the ability to control if the 
chosen interface exists, the send_command() function is used. In that way the script check after each send command
whether an error occurred - an output starting with % was generated. If so, the script exits.
'''
import netmiko
import sys
# in Linux use:
# sys.path.append('/home/kurs/python4networkers/Credentials')
# import switch_credentials
from Credentials import switch_credentials

int_name = 'int g1/0/3'

commands = [int_name, "descr netmiko no", "int g1/0/22", "descr netmiko dr."]


def conf_device(dest_device):
    # setup connection
    with netmiko.ConnectHandler(**dest_device) as net_connect:
        output = ''
        net_connect.config_mode()                                           # send "conf t"
        for command in commands:
            prompt = net_connect.find_prompt()
            output += net_connect.send_command(command, expect_string="#")
            if '%' in output:                                           # if % is found somthing is wrong
                print('Somthing went wrong - script aborted due to')    # message
                print(prompt, command)                                  # command causing an error including prompt
                print(output)                                           # error message from device
                sys.exit()                                              # exit the script
        net_connect.exit_config_mode()                                  # after all commands are sent leave config mode
        save = net_connect.save_config()                                # wr
        print('wr:\n', save)
        print(output)


if __name__ == "__main__":
    # load the dictionary with credentials from file 'Credentials/switch_credentials'
    device_list = switch_credentials.dev_list['devices']
    # loop through the list of devices
    for device in device_list:
        # actions to perform on the device
        print('Working on', device['host'])
        conf_device(device)
    print('all done')