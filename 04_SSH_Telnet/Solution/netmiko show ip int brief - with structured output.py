# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

'''
Goal:
This script demonstrates the use of netmiko to get information from Cisco switches using ssh
Details:
The list of devices is stored in a file outside the script and imported.
The script loops over all device listed in the mentioned file
On each device the command 'sh ip int brief' is performed. The command output is converted into structured Python
objects lists and dictionaries, to do so textfsm included in netmiko is used. 
'''
import netmiko
import json
from Credentials import switch_credentials


def print_title(title):
    print('\n')
    print(title)
    print('-' * len(title))


def sh_ip_int_brief(device):
    # set a command
    show_command = 'show ip interface brief | ex una'

    # set up the ssh connection
    with netmiko.ConnectHandler(**device) as net_connect:
        # get the show ip interface brief in a structured manner -> use_textfsm=True
        interfaces = net_connect.send_command(show_command, use_textfsm=True)
        print_title('Type of var interfaces')
        print(type(interfaces))

    # store all ip in the list ip_addresses
    ip_addresses = []
    for interface in interfaces:
        ip_addresses.append(interface['ip_address'])
    # the above three lines in one line: ip_addresses = (interface['ip_address'] for interface in interfaces)

    # print the interfaces in json format
    print_title('List of interfaces in json format')
    print(json.dumps(interfaces, indent=4))

    # print the ip addresses
    print_title('All IP addresses')
    for ip_address in ip_addresses:
        print(ip_address)


if __name__ == "__main__":
    # load the dictionary with credentials from file 'Credentials/switch_credentials'
    device_list = switch_credentials.dev_list['devices']
    # loop through the list of devices
    for device in device_list:
        # actions to perform on the device
        print('Working on', device['host'])
        sh_ip_int_brief(device)
