# -*- coding: utf-8 -*-
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

'''
Goal:
This script gets the running-config off all devices listed in the file Credentials/switch_credentials.py
The running-config is stored in a text file 
'''
import netmiko
import json
from datetime import datetime
from Credentials import switch_credentials


def print_title(title):
    print('\n')
    print(title)
    print('-' * len(title))


def get_time():
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
    return dt_string


def write_to_file(run_conf, host):
    out_file_name = 'outfiles/host_' + host + '_' + get_time() + '.cfg'
    out_file = open(out_file_name, 'w')
    out_file.write(run_conf)
    out_file.close()


def get_running_config(device):
    # set a command
    show_command = 'show run brief'
    find_hostname = 'sh run brief | inc ^hostname'

    # set up the ssh connection
    with netmiko.ConnectHandler(**device) as net_connect:
        # get sh run and sh run | in hostname
        line_hostname = net_connect.send_command(find_hostname)
        running_config = net_connect.send_command(show_command)
        print_title('Hostname')
        hostname = (line_hostname.split(' '))[1]
        print(line_hostname)
        print(hostname)

    return running_config, hostname


if __name__ == "__main__":
    # load the dictionary with credentials from file 'Credentials/switch_credentials'
    device_list = switch_credentials.dev_list['devices']
    # loop through the list of devices
    for device in device_list:
        # actions to perform on the device
        print('Working on', device['host'])
        running_config, hostname = get_running_config(device)
        write_to_file(running_config, hostname)

