#!/usr/bin/env python
__author__ = 'Edi, AnyWeb AG'
__Python_Version__ = '3.x'

from lxml import etree


def xml_find(xml_obj, filter_string, title):
    print(title)
    print('-' * len(title))
    result = xml_obj.find(filter_string)
    print(result.text)
    print('')


def xml_findall(xml_obj, filter_string, title):
    print(title)
    print('-' * len(title))
    result = xml_obj.findall(filter_string)
    for r in result:
        if r.text.strip():  # Remove "Empty Lines" --> Spaces an CR only
            print(r.text)
    print('')
    

def main():
    infile = "infiles/Address_List.xml"
    personen_file = open(infile,"r")
    personen_xml_str = personen_file.read()
    print('Type of personen_xml_str: ',(type(personen_xml_str)))
    personen_xml_obj = etree.fromstring(personen_xml_str)
    print('Type of personen_xml_obj: ',(type(personen_xml_obj)))
    
    # print the entire content
    print('\n\nEntire content:')
    print('---------------')
    print(etree.tostring(personen_xml_obj))
    print('')
    
    # print names only
    filter_string = './/name'
    title = 'names only'
    xml_findall(personen_xml_obj,filter_string,title)
    
    # print date
    filter_string = './date'
    title = 'date'
    xml_find(personen_xml_obj,filter_string,title)
    
    # print all attributes of all persons
    filter_string = './person//'
    title = 'all attributes of all persons'
    xml_findall(personen_xml_obj,filter_string,title)
    
    # print phone numbers of person with vorname 'Brigitte'
    filter_string = './person[vorname="Brigitte"]/phone/'
    title = 'all phone numbers of person with vorname "Brigitte"'
    xml_findall(personen_xml_obj,filter_string,title)    


if __name__ == "__main__":
    main()
